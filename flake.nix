{
  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixos-22.05";
    flake-parts.url = "github:hercules-ci/flake-parts";
    fenix = {
      url = "github:nix-community/fenix";
      inputs.nixpkgs.follows = "nixpkgs";
    };
    nix-filter.url = "github:numtide/nix-filter";
  };

  outputs = {
    self,
    nixpkgs,
    flake-parts,
    fenix,
    nix-filter,
  }:
    flake-parts.lib.mkFlake {inherit self;} {
      systems = [
        "x86_64-linux"
        "aarch64-linux"
      ];

      flake.overlays.default = final: prev: let
        src = nix-filter.lib {
          root = ./.;
          exclude = [
            (nix-filter.lib.matchExt "nix")
          ];
        };

        buildSystem = final.stdenv.buildPlatform.system;
        targetConfig = final.stdenv.targetPlatform.config;
      in {
        _toolchain_dev = with fenix.packages.${buildSystem};
          combine [
            (complete.withComponents [
              "rustc"
              "cargo"
              "rust-src"
              "clippy"
              "rustfmt"
            ])
            targets.${targetConfig}.latest.rust-std
          ];

        ldix-rs =
          (final.makeRustPlatform {
            cargo = final._toolchain_dev;
            rustc = final._toolchain_dev;
          })
          .buildRustPackage {
            inherit src;
            pname = "ldix-rs";
            version = "0.dev";
            cargoLock.lockFile = ./Cargo.lock;
            target = targetConfig;
            CARGO_BUILD_TARGET = targetConfig;
            RUST_SRC_PATH = "${final._toolchain_dev}/lib/rustlib/src/rust/library";
          };
      };

      perSystem = {
        system,
        pkgs,
        ...
      }: {
        _module.args.pkgs = import nixpkgs {
          inherit system;
          overlays = [self.overlays.default];
        };

        devShells.extra = with pkgs;
          mkShellNoCC {
            name = "extra";
            packages = [
              rust-analyzer
              lldb
              elfutils
            ];
          };

        legacyPackages = pkgs;
      };
    };
}
