use std::error::Error;
use std::path::Path;
use std::process::Command;
use std::string::FromUtf8Error;

const FILENAME: &str = "binaries/node-v18.12.0-linux-arm64/bin/npm";

fn main() -> Result<(), Box<dyn Error>> {
    let _filename = match std::env::var("FILE") {
        Err(_) => String::from(FILENAME),
        Ok(what) => what,
    };

    let path = Path::new(&_filename);
    let data = std::fs::read(path)?;

    let elf = goblin::elf::Elf::parse(&data)?;

    for lib in elf.libraries {
        println!("finding: {lib:?}");
        let y = find_lib(lib);
        println!("==> {y:?}");
        // panic!("PANICKING");
    }

    Ok(())
}

#[derive(Debug)]
enum FindlibError {
    OtherError(),
    NoResult(),
}

impl From<std::io::Error> for FindlibError {
    fn from(e: std::io::Error) -> FindlibError {
        todo!();
    }
}

impl From<FromUtf8Error> for FindlibError {
    fn from(e: FromUtf8Error) -> FindlibError {
        todo!()
    }
}

fn find_lib(lib: &str) -> Result<String, FindlibError> {
    let output = String::from_utf8(
        Command::new("nix-locate")
            .args([
                "--at-root",
                "--minimal",
                "--top-level",
                "--whole-name",
                &format!("/lib/{}", lib),
            ])
            .output()?
            .stdout,
    )?;

    let results: &Vec<_> = &output.split('\n').filter(|&s| !s.is_empty()).collect();

    let shortest = *results
        .iter()
        .min_by(|&&s1, &&s2| s1.cmp(s2))
        .ok_or(FindlibError::NoResult())?;

    Ok(String::from(shortest))
}
